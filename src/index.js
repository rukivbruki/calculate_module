import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';
import App from './App';
import Print from './Print';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'));
registerServiceWorker();

ReactDOM.render(
  <Provider store={store}>
    <Print />
  </Provider>,
  document.getElementById('print-react'));
registerServiceWorker();

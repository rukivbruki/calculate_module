import { fromJS } from 'immutable';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import {
  scroller,
  scrollSpy,
} from 'react-scroll';

import CalculatorStep from '../../components/CalculatorStep';
import StepTitle from '../../components/StepTitle';
import Select from '../../components/Select';

class SelectVariantHeight extends PureComponent {
  componentDidMount() {
    scrollSpy.update();
  }

  handleSelected = (item) => {
    this.props.updateSides({
      height: item,
    });
    setTimeout(
      scroller.scrollTo('SelectMaterialsStep', {
        duration: 500,
        delay: 100,
        smooth: 'easeInOutQuart',
      }),
      100,
    );
  };

  render() {
    const { currentType, currentSide } = this.props;
    const variantHeight = currentType.get('variantHeight', fromJS([]));
    const label = currentSide.get('height') ? currentSide.get('height') : 'Выберите высоту';

    if (variantHeight.isEmpty()) {
      return null;
    }
    return (
      <CalculatorStep>
        <StepTitle className="section-calculator__step-title_offset-bottom">
          ШАГ 2 - Выберите высоту забора
        </StepTitle>
        <Select
          items={variantHeight}
          currentItem={label}
          onSelected={this.handleSelected}
        />
        <div className="SelectMaterialsStep"></div>
      </CalculatorStep>
    );
  }
}


SelectVariantHeight.propTypes = {
  currentSide: ImmutablePropTypes.map,
  currentType: ImmutablePropTypes.map,
  updateSides: PropTypes.func,
};

export default SelectVariantHeight;

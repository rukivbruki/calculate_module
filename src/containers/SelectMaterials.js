import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import shortid from 'shortid';
import { scroller } from 'react-scroll';

import CalculatorStep from '../components/CalculatorStep';
import StepTitle from '../components/StepTitle';
import MaterialItem from '../components/MaterialItem';

class SelectMaterials extends PureComponent {
  handleSetMaterial = (item) => {
    this.props.updateSides({
      material: item.get('name'),
    });

    scroller.scrollTo('SelectVariantInstallationStep', {
      duration: 500,
      delay: 0,
      smooth: 'easeInOutQuart',
    });
  };

  render() {
    const { items } = this.props;
    if (items.isEmpty()) {
      return null;
    }
    return (
      <CalculatorStep>
        <StepTitle>ШАГ 3 - Выберите модификацию забора</StepTitle>
        <div className="section-calculator__row">
          {items.map((item) =>
            <MaterialItem
              key={shortid.generate()}
              inputName="SelectMaterials"
              data={item}
              onChange={this.handleSetMaterial}
              selected={item.get('name') === this.props.currentSide.get('material')}
            />,
          )}
        </div>
        <div className="SelectVariantInstallationStep"></div>
      </CalculatorStep>
    );
  }
}

SelectMaterials.propTypes = {
  items: ImmutablePropTypes.list,
  currentSide: ImmutablePropTypes.map,
  updateSides: PropTypes.func,
};

export default SelectMaterials;

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'recompose';

import SelectObject from '../../components/SelectObject';
import {
  makeSelectCurrentSide,
  makeSelectSides,
} from '../../store/side/selectors';
import { selectSide } from '../../store/side/actions';


class SelectSide extends PureComponent {
  handleSelected = (item) => {
    this.props.selectSide(item.get('id'));
  };

  render() {
    return (
      <SelectObject
        items={this.props.sides}
        currentItem={this.props.currentSide.get('label')}
        onSelected={this.handleSelected}
      />
    );
  }
}


SelectSide.propTypes = {
  sides: ImmutablePropTypes.map,
  currentSide: ImmutablePropTypes.map,
  selectSide: PropTypes.func,
};

const mapStateToProps = (state, ownProps) => {
  return createStructuredSelector({
    sides: makeSelectSides(),
    currentSide: makeSelectCurrentSide(),
  });
};


const wrapper = compose(
  connect(
    mapStateToProps,
    dispatch => bindActionCreators({
      selectSide,
    }, dispatch),
  ),
);

export default wrapper(SelectSide);

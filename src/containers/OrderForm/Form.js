import React, { PureComponent } from 'react';
import {
  Field,
  reduxForm,
} from 'redux-form/immutable';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';


import validate from './validate';
import FormField from './FormField';
import { makeSelectOrderForm } from '../../store/orderForm/selectors';

class OrderForm extends PureComponent {
  render() {
    const { handleSubmit, pristine, reset, submitting, submitSucceeded, validateError }  = this.props;

    return (
      <form
        className="form form-price__form"
        onSubmit={handleSubmit}
      >
        <Field
          id="name2"
          name="user[name]"
          type="text"
          component={FormField}
          label="Имя"
          labelClass="label_required-red"
          inputClass="input_name"
        />

        <Field
          id="phone2"
          name="user[phone]"
          type="text"
          component={FormField}
          label="Телефон"
          labelClass=""
          inputClass="input_phone"
        />

        <Field
          id="email2"
          name="user[email]"
          type="email"
          component={FormField}
          label="Email"
          labelClass="label_required-red"
          inputClass="input_email"
        />

        <Field
          id="comments2"
          name="order[comments]"
          type="text"
          component={FormField}
          label="Ваше сообщение"
          inputClass="input_textarea"
        />

        <input
          className="button button_red form__button"
          type="submit"
          value={submitSucceeded && !validateError ? 'Отправляется' : 'Отправить'}
          tabIndex="0"
          disabled={((submitting || submitSucceeded) && !validateError) ? true : false}
        />
      </form>
    );
  }
}

OrderForm = reduxForm({
  form: 'orderForm',
  validate: validate,
})(OrderForm);


OrderForm = connect(
  state => ({
    initialValues: makeSelectOrderForm(state), // pull initial values from account reducer
  }),
)(OrderForm);

export default OrderForm;

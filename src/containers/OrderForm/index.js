import ImmutablePropTypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import Modal from 'react-modal';
import axios from 'axios';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { setData } from '../../store/orderForm/actions';
import { resetSides } from '../../store/side/actions';

import Form from './Form';

const host = (process.env.NODE_ENV === 'development')
  ? 'http://localhost:4000/api/orders/create'
  : '/api/orders/create';

const customStyles = {
  content: {
    // top: '50%',
    // left: '50%',
    // right: 'auto',
    // bottom: 'auto',
    // marginRight: '-50%',
    // transform: 'translate(-50%, -50%)',
  },
};

Modal.setAppElement('#root');

class OrderForm extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      offline: false,
      error: false,
    };
  }

  submittedForm = (isSuccess, value) => {
    const { closeModal, setData, resetSides } = this.props;

    if (isSuccess) {
      const data = value
        .setIn([
          'order',
          'comments',
        ], null)
        .setIn([
          'order',
          'data',
        ], null);

      setData(data);
      resetSides();
      closeModal();
      return;
    }

    closeModal();
  };

  request = (data, isOffline = false) => {
    this.setState({ offline: false });
    const self = this;
    axios.post(host, data.toJS())
      .then(function(response) {
        self.submittedForm(true, data);
      })
      .catch(function(error) {
        self.setState({ error: true });
      });
    if (isOffline) {
      window.removeEventListener('online', (e) => {return false;});
    }
  };

  handleSubmitForm = (val) => {
    this.setState({ error: false });
    const orderData = val
      .setIn([
        'order',
        'data',
      ], this.props.sides)
      .setIn([
        'order',
        'data',
        'color',
      ], this.props.color)
      .setIn([
        'order',
        'utm_source',
      ], sessionStorage.getItem('utm_source') ? sessionStorage.getItem('utm_source').toString() : null);

    if (!navigator.onLine) {
      this.setState({ offline: true });
      window.addEventListener('online', (e) => this.request(orderData, true));
    } else {
      this.request(orderData);
    }
  };

  render() {
    return (
      <Modal
        isOpen={this.props.modalIsOpen}
        onRequestClose={this.props.closeModal}
        style={customStyles}
        contentLabel="Example Modal"
        className="LenzaborModal"
        overlayClassName="LenzaborModalOverlay"
      >
          <span
            className="LenzaborModalClose"
            onClick={this.props.closeModal}
          >×</span>
        <div>
          <div className="form-price form-price_popup zoom-anim-dialog">
            <div className="form-price__container">
              <h3
                className="form-price__heading"
                style={{ margin: '35px 32px' }}
              >
                Получить подробную смету
              </h3>

              <Form
                onSubmit={this.handleSubmitForm}
                validateError={this.state.error}
              />

              {this.state.offline &&
               <div style={{ marginTop: '16px', color: 'red', fontSize: '16px' }}>
                 Отсутствует интернет соединение!
                 <br />
                 Пожалуйста, не закрывайте окно, мы доставим ваше сообщение, как только появится сеть.
               </div>
              }

              {this.state.error &&
               <div style={{ marginTop: '16px', color: 'red', fontSize: '16px' }}>
                 Форма не может быть отправлена, так как в сообщении присутствуют ссылки. Попробуйте отправить без них!
               </div>
              }
            </div>
          </div>
        </div>
      </Modal>
    );
  }
}

OrderForm.propTypes = {
  sides: ImmutablePropTypes.map,
  color: ImmutablePropTypes.map,
  closeModal: PropTypes.func,
  setData: PropTypes.func,
  resetSides: PropTypes.func,
  modalIsOpen: PropTypes.bool,
};

const mapStateToProps = (state, ownProps) => {};

const wrapper = compose(
  connect(
    mapStateToProps,
    dispatch => bindActionCreators({
      setData,
      resetSides,
    }, dispatch),
  ),
);

export default wrapper(OrderForm);

const emailRegexp = /^(([^<>()[\]\\.,;:\s@\']+(\.[^<>()[\]\\.,;:\s@\']+)*)|(\'.+\'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;


const validate = values => {
  const errors = {
    user: {},
  };

  if (!values.getIn([
      'user',
      'name',
    ])) {
    errors.user.name = 'Обязательное поле!';
  } else if (values.getIn([
      'user',
      'name',
    ]).length < 3) {
    errors.user.name = 'Минимум 3 символа!';
  }

  if (!values.getIn([
      'user',
      'email',
    ])) {
    errors.user.email = 'Обязательное поле!';
  } else if (!emailRegexp.test(values.getIn([
      'user',
      'email',
    ]))) {
    errors.user.email = 'Некорректный email!';
  }

  return errors;
};

export default validate;

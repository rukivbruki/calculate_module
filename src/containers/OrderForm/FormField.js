import React from 'react';
import InputMask from 'react-input-mask';

const FormField = (props) => {
  const {
    id,
    input,
    label,
    labelClass,
    inputClass,
    type,
    meta: { touched, error, warning },
  } = props;

  if (inputClass === 'input_textarea') {
    return (
      <div className="form__col">
        <label className={`label ${labelClass}`} htmlFor={id}>
          <span className="label__text">{label}</span>
        </label>
        <div className="form-validation__group">
                  <textarea
                    id={id}
                    className={`input input_textarea form__input ${inputClass} ${touched && error && 'validation_invalid'}`}
                    {...input}
                  />
        </div>
      </div>
    );
  }
  if (inputClass === 'input_phone') {
    return (
      <div className="form__col">
        <label className={`label ${labelClass}`} htmlFor={id}>
          <span className="label__text">{label}</span>
        </label>
        <div className="form-validation__group">
          <InputMask
            mask="+7(999) 999-99-99"
            id={id}
            className={`input ${inputClass} form__input ${touched && error && 'validation_invalid'}`}
            {...input}
            placeholder={label}
            type={type}
          />
        </div>
      </div>
    );
  }
  return (
    <div className="form__col">
      <label className={`label ${labelClass}`} htmlFor={id}>
        <span className="label__text">{label}</span>
      </label>
      <div className="form-validation__group">
        <input
          id={id}
          className={`input ${inputClass} form__input ${touched && error && 'validation_invalid'}`}
          {...input}
          placeholder={label}
          type={type}
        />
        {touched &&
         ((error && <span className="form-validation__error-text">{error}</span>) ||
          (warning && <span>{warning}</span>))}
      </div>
    </div>
  );
};

export default FormField;

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import shortid from 'shortid';
import { scroller } from 'react-scroll';

import CalculatorStep from '../components/CalculatorStep';
import StepTitle from '../components/StepTitle';
import MaterialItem from '../components/MaterialItem';

class SelectInstallationMethod extends PureComponent {
  handleSetVariantPost = (item) => {
    this.props.updateSides({
      installationMethod: item.get('name'),
    });

    scroller.scrollTo('SelectWidthStep', {
      duration: 500,
      delay: 0,
      smooth: 'easeInOutQuart',
    });
  };

  render() {
    const { currentSide } = this.props;
    if (!currentSide.get('post')) {
      return null;
    }
    return (
      <CalculatorStep>
        <StepTitle>ШАГ 5 - Выберите метод установки</StepTitle>
        <div className="section-calculator__row">
          {this.props.items.map((item) =>
            <MaterialItem
              key={shortid.generate()}
              inputName="SelectInstallationMethod"
              data={item}
              onChange={this.handleSetVariantPost}
              selected={item.get('name') === currentSide.get('installationMethod')}
            />,
          )}
        </div>
        <div className="SelectWidthStep"></div>
      </CalculatorStep>
    );
  }
}

SelectInstallationMethod.propTypes = {
  items: ImmutablePropTypes.list,
  currentSide: ImmutablePropTypes.map,
  updateSides: PropTypes.func,
};

export default SelectInstallationMethod;

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import NumPad from 'react-numpad';
import { scroller } from 'react-scroll';

import CalculatorStep from '../components/CalculatorStep';
import StepTitle from '../components/StepTitle';

class SelectCountGate extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      value: props.currentSide.get('gateCount'),
      error: null,
    };
  }

  componentWillReceiveProps(newProps) {
    if (this.props.currentSide.get('gateCount') !== newProps.currentSide.get('gateCount')) {
      this.setState({
        value: newProps.currentSide.get('gateCount'),
      });
    }
  }

  validation = (val) => {
    if (typeof parseInt(val) !== 'number') {
      return {
        value: '',
        error: 'значение должно быть числом',
      };
    }
    if (parseInt(val) < 0) {
      return {
        value: 0,
        error: 'значение не можеть быть отрицательным',
      };
    }
    if (parseInt(val) === 0) {
      return {
        value: 0,
        error: null,
      };
    }
    return {
      value: val,
      error: null,
    };
  };

  handleChange = (e) => {
    this.handleChangeNumpad(e.target.value);
  };

  handleChangeNumpad = (val) => {
    const validationState = this.validation(val);
    this.setState(validationState);
    this.props.updateSides({
      gateCount: validationState.value,
      gateEmpty: !(parseInt(validationState.value) > 0),
    });
    scroller.scrollTo('SelectGateTypeStep', {
      duration: 500,
      delay: 0,
      smooth: 'easeInOutQuart',
    });
  };

  handleClick = (e) => {
    this.props.updateSides({
      gateCount: 0,
      gateEmpty: true,
      gateType: '',
      gateName: '',
      gateWidth: '',
    });
    scroller.scrollTo('SelectGateTypeStep', {
      duration: 500,
      delay: 0,
      smooth: 'easeInOutQuart',
    });
  };

  render() {
    const { currentSide } = this.props;
    if (!currentSide.get('width')) {
      return null;
    }
    return (
      <CalculatorStep>
        <StepTitle className='section-calculator__step-title_offset-bottom'>
          ШАГ 7 - Укажите количество ворот
        </StepTitle>
        {this.state.error && <div style={{ color: 'red', paddingLeft: 25 }}>{this.state.error}</div>}
        <div className="NumPadPositiveIntegerNumber">
          <NumPad.PositiveIntegerNumber
            onChange={this.handleChangeNumpad}
            position="startBottomLeft"
            value={this.state.value}
          />
        </div>
        {/*<input*/}
          {/*style={this.state.error && { borderColor: 'red' }}*/}
          {/*className="input section-calculator__input"*/}
          {/*type="number"*/}
          {/*name="SelectCountGate"*/}
          {/*value={this.state.value}*/}
          {/*onChange={this.handleChange}*/}
        {/*/>*/}
        <span className="section-calculator__skip" onClick={this.handleClick}>Пропустить шаг</span>
        <div className="SelectGateTypeStep"></div>
      </CalculatorStep>
    );
  }
}

SelectCountGate.propTypes = {
  currentSide: ImmutablePropTypes.map,
  updateSides: PropTypes.func,
};

export default SelectCountGate;

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import shortid from 'shortid';
import { scroller } from 'react-scroll';

import CalculatorStep from '../components/CalculatorStep';
import StepTitle from '../components/StepTitle';
import MaterialItem from '../components/MaterialItem';

class SelectVariantInstallation extends PureComponent {
  handleSetMaterial = (item) => {
    this.props.updateSides({
      variantInstallation: item.get('name'),
    });
    scroller.scrollTo('SelectMaterialPostStep', {
      duration: 500,
      delay: 0,
      smooth: 'easeInOutQuart',
    });
  };

  render() {
    const { items, currentSide } = this.props;
    if (items.isEmpty()) {
      return null;
    }
    if (!currentSide.get('material')) {
      return null;
    }
    return (
      <CalculatorStep>
        <StepTitle style={{ marginTop: 50 }}>Выберите вариант установки</StepTitle>
        <div className="section-calculator__row">
          {items.map((item) =>
            <MaterialItem
              key={shortid.generate()}
              inputName="SelectVariantInstallation"
              data={item}
              onChange={this.handleSetMaterial}
              selected={item.get('name') === currentSide.get('variantInstallation')}
            />,
          )}
        </div>
        <div className="SelectMaterialPostStep"></div>
      </CalculatorStep>
    );
  }
}

SelectVariantInstallation.propTypes = {
  items: ImmutablePropTypes.list,
  currentSide: ImmutablePropTypes.map,
  updateSides: PropTypes.func,
};

export default SelectVariantInstallation;

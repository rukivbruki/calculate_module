import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { scroller } from 'react-scroll';

import CalculatorStep from '../components/CalculatorStep';
import StepTitle from '../components/StepTitle';
import Select from '../components/Select';

class SelectGateWidth extends PureComponent {
  handleChange = (item) => {
    this.props.updateSides({
      gateWidth: item,
    });
    scroller.scrollTo('SelectCountDoorStep', {
      duration: 500,
      delay: 0,
      smooth: 'easeInOutQuart',
    });
  };

  render() {
    const { currentSide } = this.props;
    const gateTypeName = currentSide.get('gateType');
    const label = currentSide.get('gateWidth') ? currentSide.get('gateWidth') : 'Выберите ширину';

    if (currentSide.get('gateEmpty')) {
      return null;
    }
    if (!gateTypeName) {
      return null;
    }
    return (
      <CalculatorStep>
        <StepTitle className='section-calculator__step-title_offset-bottom'>
          Ширина ворот
        </StepTitle>
        <Select
          items={this.props.items}
          currentItem={label}
          onSelected={this.handleChange}
        />
        <div className="SelectCountDoorStep"></div>
      </CalculatorStep>
    );
  }
}

SelectGateWidth.propTypes = {
  items: ImmutablePropTypes.list,
  currentSide: ImmutablePropTypes.map,
  updateSides: PropTypes.func,
};

export default SelectGateWidth;

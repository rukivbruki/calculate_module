import { fromJS } from 'immutable';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import shortid from 'shortid';

import CalculatorStep from '../../components/CalculatorStep';
import StepTitle from '../../components/StepTitle';
import MaterialItem from '../../components/MaterialItem';

class Materials extends PureComponent {
  handleSetMaterial = (item) => {
    this.props.updateSides({
      [this.props.currentSide.get('id')]: {
        material: item.get('name'),
      },
    });
  };

  handleSetVariantInstallation = (item) => {
    this.props.updateSides({
      [this.props.currentSide.get('id')]: {
        variantInstallation: item.get('name'),
      },
    });
  };

  render() {
    const variantInstallation = this.props.currentType.get('variantInstallation', fromJS({}));

    if (this.props.materials.isEmpty()) {
      return null;
    }
    return (
      <CalculatorStep>
        <StepTitle>ШАГ 3 - Выберите модификацию забора</StepTitle>
        <div className="section-calculator__row">
          {this.props.materials.entrySeq().map(([key, item]) =>
            <MaterialItem
              key={shortid.generate()}
              inputName="Materials"
              data={item}
              onChange={this.handleSetMaterial}
              selected={item.get('name') === this.props.currentSide.get('material')}
            />,
          )}
        </div>

        {!variantInstallation.isEmpty() && !this.props.currentMaterial.isEmpty() &&
         <div>
           <StepTitle style={{ marginTop: 50 }}>Выберите вариант установки</StepTitle>
           <div className="section-calculator__row">
             {variantInstallation.entrySeq().map(([key, item]) =>
               <MaterialItem
                 key={shortid.generate()}
                 inputName="variantInstallation"
                 data={item}
                 onChange={this.handleSetVariantInstallation}
                 selected={item.get('name') === this.props.currentSide.get('variantInstallation')}
               />,
             )}
           </div>
         </div>
        }
      </CalculatorStep>
    );
  }
}

Materials.propTypes = {
  materials: ImmutablePropTypes.map,
  currentMaterial: ImmutablePropTypes.map,
  currentType: ImmutablePropTypes.map,
  currentSide: ImmutablePropTypes.map,
  updateSides: PropTypes.func,
};

export default Materials;

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import shortid from 'shortid';
import { scroller } from 'react-scroll';

import CalculatorStep from '../components/CalculatorStep';
import StepTitle from '../components/StepTitle';
import MaterialItem from '../components/MaterialItem';

class SelectMaterialPost extends PureComponent {
  handleSetVariantPost = (item) => {
    this.props.updateSides({
      post: item.get('name'),
    });
    scroller.scrollTo('SelectInstallationMethodStep', {
      duration: 500,
      delay: 0,
      smooth: 'easeInOutQuart',
    });
  };

  render() {
    const { currentSide } = this.props;
    if (!currentSide.get('variantInstallation') && !currentSide.get('material')) {
      return null;
    }
    return (
      <CalculatorStep>
        <StepTitle>ШАГ 4 - Выберите тип столба</StepTitle>
        <div className="section-calculator__row">
          {this.props.items.entrySeq().map(([key, item]) =>
            <MaterialItem
              key={shortid.generate()}
              inputName="SelectMaterialPost"
              data={item}
              onChange={this.handleSetVariantPost}
              selected={item.get('name') === currentSide.get('post')}
            />,
          )}
        </div>
        <div className="SelectInstallationMethodStep"></div>
      </CalculatorStep>
    );
  }
}

SelectMaterialPost.propTypes = {
  items: ImmutablePropTypes.map,
  currentSide: ImmutablePropTypes.map,
  updateSides: PropTypes.func,
};

export default SelectMaterialPost;

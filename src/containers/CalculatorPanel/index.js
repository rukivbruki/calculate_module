import { fromJS } from 'immutable';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'recompose';
import NumberFormat from 'react-number-format';
import ReactTooltip from 'react-tooltip';

import OrderForm from '../OrderForm';
import SelectColor from '../../components/SelectColor';

import {
  makeSelectSides,
  makeSelectSum,
  makeSelectColor,
} from '../../store/side/selectors';
import { makeSelectColors } from '../../store/materials/selectors';
import {
  addSide,
  removeSide,
  selectSide,
  updateSides,
  resetSides,
  setColor,
} from '../../store/side/actions';


class CalculatorPanel extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      sides: fromJS({}),
      modalIsOpen: false,
    };
  }

  handleOpenModal = () => {
    this.setState({
      modalIsOpen: true,
    });
  };

  handleCloseModal = () => {
    this.setState({
      modalIsOpen: false,
    });
  };

  handleSetEditSide = (side) => {
    const currentSideData = side.mergeDeep(fromJS({ editing: true }));
    const payload = fromJS({
      currentSideId: side.get('id'),
      data: currentSideData,
    });
    this.setState({
      sides: this.state.sides.set(side.get('id'), side.get('label')),
    });
    this.props.updateSides(payload);
  };

  handleUpdateSide = (side) => {
    const currentSideData = side
      .mergeDeep(
        fromJS(
          {
            editing: false,
            label: this.state.sides.get(side.get('id')),
          },
        ),
      );
    const payload = fromJS({
      currentSideId: side.get('id'),
      data: currentSideData,
    });
    this.props.updateSides(payload);
  };

  handleChangeLabel = (e, side) => {
    this.setState({
      sides: this.state.sides.set(side.get('id'), e.target.value),
    });
  };

  handlePrint = (e) => {
    window.print();
  };

  render() {
    return (
      <div className="section-calculator__panel section-calculator__panel_sticky">
        <div className="calculator-panel section-calculator__calculator-panel">
          {this.props.sides.entrySeq().map(([key, item]) =>
            <div key={key} className="calculator-panel__side">
              <div className="calculator-panel__name">
                {!item.get('editing') &&
                 <label
                   className="calculator-panel__label"
                   htmlFor={`input${key}`}
                   onClick={() => this.props.selectSide(item.get('id'))}
                 >
                   {item.get('label')}
                 </label>
                }
                {item.get('editing') &&
                 <input
                   className="calculator-panel__input"
                   type="text"
                   name={`input${key}`}
                   value={this.state.sides.get(item.get('id'))}
                   id={`input${key}`}
                   onChange={(e) => this.handleChangeLabel(e, item)}
                 />
                }
                <div className="calculator-panel__controls">
                  {!item.get('editing') &&
                   <ReactTooltip id='btn_edit' place="top" type="dark" effect="solid">
                     <div style={{ width: 278, textAlign: 'center' }}>
                       Вы можете задать произвольное название стороны забора
                     </div>
                   </ReactTooltip>
                  }
                  {!item.get('editing') &&
                   <span
                     data-tip
                     data-for='btn_edit'
                     className="calculator-panel__control-btn calculator-panel__control-btn_edit"
                     onClick={() => this.handleSetEditSide(item)}
                   />
                  }
                  {!item.get('editing') && this.props.sides.size > 1 &&
                   <ReactTooltip id='btn_delete' place="top" type="dark" effect="solid">
                     Удалить
                   </ReactTooltip>
                  }
                  {!item.get('editing') && this.props.sides.size > 1 &&
                   <span
                     data-tip
                     data-for='btn_delete'
                     className="calculator-panel__control-btn calculator-panel__control-btn_delete"
                     onClick={() => this.props.removeSide(item.get('id'))}
                   />
                  }
                  {item.get('editing') &&
                   <ReactTooltip id='btn_save' place="top" type="dark" effect="solid">
                     Сохранить
                   </ReactTooltip>
                  }
                  {item.get('editing') &&
                   <span
                     data-tip
                     data-for='btn_save'
                     className="calculator-panel__control-btn calculator-panel__control-btn_save"
                     onClick={() => this.handleUpdateSide(item)}
                   />
                  }
                </div>
              </div>
              <div className="calculator-panel__results">
                <div className="calculator-panel__item">
                  <div className="calculator-panel__result-name">Работа:</div>
                  <div className="calculator-panel__result-price">
                    {item.getIn([
                      'estimate',
                      'priceWorksSum',
                    ]) ? <span className="calculator-panel__result-price-i">
                      <NumberFormat
                        value={item.getIn([
                          'estimate',
                          'priceWorksSum',
                        ])}
                        thousandSeparator=' '
                        displayType={'text'}
                      /> р.
                    </span> : <span className="calculator-panel__result-price-i calculator-panel__result-price-i_loading" />
                    }
                  </div>
                </div>
                <div className="calculator-panel__item">
                  <div className="calculator-panel__result-name">Материалы:</div>
                  <div className="calculator-panel__result-price">
                    {item.getIn([
                      'estimate',
                      'priceMaterialsSum',
                    ]) ? <span className="calculator-panel__result-price-i">
                      <NumberFormat
                        value={item.getIn([
                          'estimate',
                          'priceMaterialsSum',
                        ])}
                        thousandSeparator=' '
                        displayType={'text'}
                      /> р.
                    </span> : <span className="calculator-panel__result-price-i calculator-panel__result-price-i_loading" />
                    }
                  </div>
                </div>
              </div>
            </div>,
          )}

          <div className="calculator-panel__side calculator-panel__side_color">
            <div className="label label_no-left-offset calculator-panel__label-color">
              Цвет забора
            </div>
            <SelectColor
              currentItem={this.props.color}
              items={this.props.colors}
              onSelected={(color) => this.props.setColor(color)}
            />
          </div>

          <div className="calculator-panel__row">
            <div
              style={{ cursor: 'pointer' }}
              className="calculator-panel__add-side-btn"
              onClick={this.props.addSide}
            >
              <span>Добавить сторону</span>
            </div>
            <span
              style={{ cursor: 'pointer' }}
              className="calculator-panel__visualizer-btn"
            >
              <span>Визуализатор</span>
            </span>
          </div>
          <div className="calculator-panel__bottom">
            <ReactTooltip
              id='total-price'
              type="dark"
              effect="solid"
              multiline={true}
            >
              <div style={{ width: 278 }}>
                Любые расчеты в сервисе &quot;Калькулятор&quot; носят исключительно информационный характер,
                не являются предложением оферты и могут отличаться от расчетов в офисе или удаленных расчетов,
                произведенных нашим менеджером. Более точные расчеты будут предоставлены Вам после отправки
                запроса на получение подробной сметы. Спасибо!
              </div>
            </ReactTooltip>
            <div
              data-tip
              data-for='total-price'
              className="calculator-panel__total-price"
            >
              <span className="calculator-panel__total-price-val">
                Итого:
                <NumberFormat
                  value={this.props.sum}
                  thousandSeparator=' '
                  displayType={'text'}
                />
                р.
              </span>
            </div>

            {this.props.sum > 0 &&
             <span
               style={{ cursor: 'pointer' }}
               className="button button_gray calculator-panel__button"
               onClick={this.handleOpenModal}
             >
                Получить смету
              </span>
            }

            <div style={{ marginTop: 20 }}>

              <ReactTooltip id='reset' place="top" type="dark" effect="solid">
                Сбросить все расчеты?
              </ReactTooltip>
              <span
                data-tip
                data-for='reset'
                style={{ cursor: 'pointer' }}
                className="calculator-panel__second-btn calculator-panel__second-btn_reset"
                onClick={() => this.props.resetSides()}
              >Сбросить</span>

              <span
                style={{ cursor: 'pointer' }}
                className="calculator-panel__second-btn calculator-panel__second-btn_print"
                onClick={this.handlePrint}
              >
                Печать
              </span>
            </div>

          </div>
        </div>

        <OrderForm
          modalIsOpen={this.state.modalIsOpen}
          closeModal={this.handleCloseModal}
          sides={this.props.sides}
          color={this.props.color}
        />
      </div>
    );
  }
}

CalculatorPanel.propTypes = {
  color: ImmutablePropTypes.map,
  sides: ImmutablePropTypes.map,
  addSide: PropTypes.func,
  removeSide: PropTypes.func,
  selectSide: PropTypes.func,
  updateSides: PropTypes.func,
  sendServer: PropTypes.func,
  resetSides: PropTypes.func,
  setColor: PropTypes.func,
  sum: PropTypes.number,
};

const mapStateToProps = (state, ownProps) => {
  return createStructuredSelector({
    sides: makeSelectSides(),
    sum: makeSelectSum(),
    colors: makeSelectColors(),
    color: makeSelectColor(),
  });
};


const wrapper = compose(
  connect(
    mapStateToProps,
    dispatch => bindActionCreators({
      addSide,
      removeSide,
      selectSide,
      updateSides,
      resetSides,
      setColor,
    }, dispatch),
  ),
);

export default wrapper(CalculatorPanel);

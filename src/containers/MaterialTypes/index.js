import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import shortid from 'shortid';
import { scroller } from 'react-scroll';

import CalculatorStep from '../../components/CalculatorStep';
import StepTitle from '../../components/StepTitle';
import MaterialItem from '../../components/MaterialItem';


class MaterialTypes extends PureComponent {
  handleSetType = (item) => {
    const { currentSide, updateSides } = this.props;

    updateSides({
      type: item.get('name'),
      id: currentSide.get('id'),
      label: currentSide.get('label'),
      editing: currentSide.get('editing'),
    }, true);

    scroller.scrollTo('SelectVariantHeightStep', {
      duration: 500,
      delay: 100,
      smooth: 'easeInOutQuart',
    });
  };

  render() {
    return (
      <CalculatorStep>
        <StepTitle>
          ШАГ 1 - Выберите тип забора
        </StepTitle>

        <div className="section-calculator__row">
          {this.props.items.entrySeq().map(([key, item]) =>
            <MaterialItem
              key={shortid.generate()}
              inputName='MaterialTypes'
              data={item}
              onChange={this.handleSetType}
              selected={item.get('name') === this.props.currentSide.get('type')}
            />,
          )}
        </div>
        <div className="SelectVariantHeightStep"></div>
      </CalculatorStep>
    );
  }
}


MaterialTypes.propTypes = {
  items: ImmutablePropTypes.map,
  currentSide: ImmutablePropTypes.map,
  updateSides: PropTypes.func,
};

export default MaterialTypes;

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import NumPad from 'react-numpad';
import { scroller } from 'react-scroll';

import CalculatorStep from '../components/CalculatorStep';
import StepTitle from '../components/StepTitle';

class SelectCountDoor extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      value: props.currentSide.get('doorCount'),
      error: null,
    };
  }

  componentWillReceiveProps(newProps) {
    if (this.props.currentSide.get('doorCount') !== newProps.currentSide.get('doorCount')) {
      this.setState({
        value: newProps.currentSide.get('doorCount'),
      });
    }
  }

  validation = (val) => {
    if (typeof parseInt(val) !== 'number') {
      return {
        value: 0,
        error: 'значение должно быть числом',
      };
    }
    if (parseInt(val) < 0) {
      return {
        value: 0,
        error: 'значение не можеть быть отрицательным',
      };
    }
    return {
      value: val,
      error: null,
    };
  };

  handleChange = (e) => {
    this.handleChangeNumpad(e.target.value);
  };

  handleChangeNumpad = (val) => {
    const validationState = this.validation(val);
    this.setState(validationState);
    this.props.updateSides({
      doorCount: validationState.value,
      doorEmpty: !(parseInt(validationState.value) > 0),
    });
    scroller.scrollTo('SelectDoorStep', {
      duration: 500,
      delay: 0,
      smooth: 'easeInOutQuart',
    });
  };


  handleClick = (e) => {
    this.props.updateSides({
      doorCount: 0,
      doorEmpty: true,
      doorName: '',
      lockName: '',
    });
    scroller.scrollTo('SelectDoorStep', {
      duration: 500,
      delay: 0,
      smooth: 'easeInOutQuart',
    });
  };

  render() {
    const { currentSide } = this.props;
    if (!currentSide.get('gateWidth') && !currentSide.get('gateEmpty')) {
      return null;
    }
    return (
      <CalculatorStep>
        <StepTitle className='section-calculator__step-title_offset-bottom'>
          ШАГ 9 - Укажите количество калиток
        </StepTitle>
        {this.state.error && <div style={{ color: 'red', paddingLeft: 25 }}>{this.state.error}</div>}
        <div className="NumPadPositiveIntegerNumber">
          <NumPad.PositiveIntegerNumber
            onChange={this.handleChangeNumpad}
            position="startBottomLeft"
            value={this.state.value}
          />
        </div>
        {/*<input*/}
          {/*style={this.state.error && { borderColor: 'red' }}*/}
          {/*className="input section-calculator__input"*/}
          {/*type="number"*/}
          {/*name="SelectCountDoor"*/}
          {/*value={this.state.value}*/}
          {/*onChange={this.handleChange}*/}
        {/*/>*/}
        <span className="section-calculator__skip" onClick={this.handleClick}>Пропустить шаг</span>
        <div className="SelectDoorStep"></div>
      </CalculatorStep>
    );
  }
}

SelectCountDoor.propTypes = {
  currentSide: ImmutablePropTypes.map,
  updateSides: PropTypes.func,
};

export default SelectCountDoor;

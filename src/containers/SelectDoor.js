import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import shortid from 'shortid';
import { scroller } from 'react-scroll';

import CalculatorStep from '../components/CalculatorStep';
import StepTitle from '../components/StepTitle';
import MaterialItem from '../components/MaterialItem';

class SelectDoor extends PureComponent {
  handleSetVariantPost = (item) => {
    this.props.updateSides({
      doorName: item.get('name'),
    });
    scroller.scrollTo('SelectLockStep', {
      duration: 500,
      delay: 0,
      smooth: 'easeInOutQuart',
    });
  };

  render() {
    const { currentSide } = this.props;
    if (currentSide.get('doorEmpty')) {
      return null;
    }
    if (!currentSide.get('doorCount')) {
      return null;
    }
    return (
      <CalculatorStep>
        <StepTitle>
          ШАГ 10 - Выберите калитку
        </StepTitle>
        <div className="section-calculator__row">
          {this.props.items.map((item) =>
            <MaterialItem
              key={shortid.generate()}
              inputName="SelectDoor"
              data={item}
              onChange={this.handleSetVariantPost}
              selected={item.get('name') === currentSide.get('doorName')}
            />,
          )}
        </div>
        <div className="SelectLockStep"></div>
      </CalculatorStep>
    );
  }
}

SelectDoor.propTypes = {
  items: ImmutablePropTypes.list,
  currentSide: ImmutablePropTypes.map,
  updateSides: PropTypes.func,
};

export default SelectDoor;

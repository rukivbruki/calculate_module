import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import shortid from 'shortid';

import CalculatorStep from '../components/CalculatorStep';
import StepTitle from '../components/StepTitle';
import MaterialItem from '../components/MaterialItem';

class SelectLock extends PureComponent {
  handleSetVariantPost = (item) => {
    this.props.updateSides({
      lockName: item.get('name'),
    });
  };

  render() {
    const { currentSide } = this.props;
    if (currentSide.get('doorEmpty')) {
      return <div className="SelectLock"></div>;
    }
    if (!currentSide.get('doorName')) {
      return <div className="SelectLock"></div>;
    }
    return (
      <CalculatorStep className="SelectLock">
        <StepTitle>
          ШАГ 11 - Выберите замок для калитки
        </StepTitle>
        <div className="section-calculator__row">
          {this.props.items.map((item) =>
            <MaterialItem
              key={shortid.generate()}
              inputName="SelectLock"
              data={item}
              onChange={this.handleSetVariantPost}
              selected={item.get('name') === currentSide.get('lockName')}
            />,
          )}
        </div>
      </CalculatorStep>
    );
  }
}

SelectLock.propTypes = {
  items: ImmutablePropTypes.list,
  currentSide: ImmutablePropTypes.map,
  updateSides: PropTypes.func,
};

export default SelectLock;

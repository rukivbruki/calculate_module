import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import NumPad from 'react-numpad';
import { scroller } from 'react-scroll';

import CalculatorStep from '../components/CalculatorStep';
import StepTitle from '../components/StepTitle';

class SelectWidth extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      value: props.currentSide.get('width'),
      error: null,
    };
  }

  componentWillReceiveProps(newProps) {
    if (this.props.currentSide.get('width') !== newProps.currentSide.get('width')) {
      this.setState({
        value: newProps.currentSide.get('width'),
      });
    }
  }

  validation = (val) => {
    if (typeof parseInt(val) !== 'number') {
      return {
        value: 0,
        error: 'значение должно быть числом',
      };
    }
    if (parseInt(val) <= 0) {
      return {
        value: 0,
        error: 'значение не можеть быть отрицательным или равно 0',
      };
    }
    return {
      value: val,
      error: null,
    };
  };

  handleChange = (e) => {
    this.handleChangeNumpad(e.target.value);
  };

  handleChangeNumpad = (val) => {
    const validationState = this.validation(val);
    this.setState(validationState);
    this.props.updateSides({
      width: validationState.value,
    });
    scroller.scrollTo('SelectCountGateStep', {
      duration: 500,
      delay: 0,
      smooth: 'easeInOutQuart',
    });
  };

  render() {
    const { currentSide } = this.props;
    if (!currentSide.get('installationMethod')) {
      return null;
    }
    return (
      <CalculatorStep>
        <StepTitle className='section-calculator__step-title_offset-bottom'>
          ШАГ 6 - Укажите общую длину забора с калиткой и воротами, пог. м.
        </StepTitle>
        {this.state.error && <div style={{ color: 'red', paddingLeft: 25 }}>{this.state.error}</div>}
        <div className="NumPadPositiveIntegerNumber">
          <NumPad.PositiveIntegerNumber
            onChange={this.handleChangeNumpad}
            position="startBottomLeft"
            value={this.state.value}
          />
        </div>

        {/*<input*/}
          {/*style={this.state.error && { borderColor: 'red' }}*/}
          {/*className="input section-calculator__input"*/}
          {/*type="number"*/}
          {/*name="SelectWidth"*/}
          {/*value={this.state.value}*/}
          {/*onChange={this.handleChange}*/}
        {/*/>*/}
        <div className="SelectCountGateStep"></div>
      </CalculatorStep>
    );
  }
}

SelectWidth.propTypes = {
  currentSide: ImmutablePropTypes.map,
  updateSides: PropTypes.func,
};

export default SelectWidth;

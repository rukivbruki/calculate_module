import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import shortid from 'shortid';
import { scroller } from 'react-scroll';

import CalculatorStep from '../components/CalculatorStep';
import StepTitle from '../components/StepTitle';
import MaterialItem from '../components/MaterialItem';

class SelectGateType extends PureComponent {
  handleSetVariantPost = (item) => {
    if (item.get('variantWidth')) {
      this.props.updateSides({
        gateType: item.get('name'),
        gateName: '',
        gateWidth: '',
      });
    } else {
      this.props.updateSides({
        gateType: '',
        gateName: item.get('name'),
        gateWidth: item.get('width'),
      });
    }
    scroller.scrollTo('SelectGateWidthStep', {
      duration: 500,
      delay: 0,
      smooth: 'easeInOutQuart',
    });
  };

  render() {
    const { currentSide } = this.props;
    if (!(parseInt(currentSide.get('gateCount')) > 0)) {
      return null;
    }
    return (
      <CalculatorStep>
        <StepTitle>
          ШАГ 8 - Выберите ворота
        </StepTitle>
        <div className="section-calculator__row">
          {this.props.items.map((item) =>
            <MaterialItem
              key={shortid.generate()}
              inputName="SelectGateType"
              data={item}
              onChange={this.handleSetVariantPost}
              selected={item.get('name') === currentSide.get('gateType') || item.get('name') === currentSide.get('gateName')}
            />,
          )}
        </div>
        <div className="SelectGateWidthStep"></div>
      </CalculatorStep>
    );
  }
}

SelectGateType.propTypes = {
  items: ImmutablePropTypes.list,
  currentSide: ImmutablePropTypes.map,
  updateSides: PropTypes.func,
};

export default SelectGateType;

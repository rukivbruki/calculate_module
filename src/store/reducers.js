import { combineReducers } from 'redux-immutable';
import { reducer as form } from 'redux-form/immutable';

import sideReducer from './side/reducer';
import materialsReducer from './materials/reducer';
import typesReducer from './types/reducer';
import orderFormReducer from './orderForm/reducer';

export default combineReducers({
  side: sideReducer,
  materials: materialsReducer,
  types: typesReducer,
  orderForm: orderFormReducer,
  form: form,
});

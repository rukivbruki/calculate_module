import { fromJS } from 'immutable';
import { createSelector } from 'reselect';
import { makeSelectCurrentSide } from '../side/selectors';
import {
  makeSelectMaterialsDomain,
  makeSelectCurrentSidePost,
  makeSelectMaterialsLaga,
} from '../materials/selectors';
import {
  makeSelectCurrentSideMaterial,
  makeSelectCurrentSideGate,
  makeSelectCurrentSideDoor,
  makeSelectCurrentSideLock,
} from '../types/selectors';
import { makeSelectCountPost } from './base';
import { makeSelectPerimeter } from './perimeter';
import {
  TYPE_1,
  TYPE_2,
  TYPE_3,
  TYPE_4,
  TYPE_5,
  TYPE_6,
  TYPE_7,
  TYPE_8,
  TYPE_FIXINGS,
  TYPE_STONE,
  TYPE_SAND,
  TYPE_CEMENT,
  VARIANT_INSTALLATION_1,
  VARIANT_INSTALLATION_2,
  VARIANT_INSTALLATION_3,
  VARIANT_INSTALLATION_4,
} from '../../data/constants';
import {
  computeEntity,
  computeResult,
} from './helpers';


/**
 * кол-во Основного материала(штакетник, профнастил и т.д.)
 */
const calculateMaterialBase = () => createSelector(
  makeSelectCurrentSide(),
  makeSelectCurrentSideMaterial(),
  makeSelectPerimeter(),
  (currentSide, material, perimeter) => {
    const type = currentSide.get('type'); // тип забора
    const variantInstallation = currentSide.get('variantInstallation'); // вариант установки
    let quantity = 0; // количество базового материала

    if (type === TYPE_1) {
      quantity = perimeter / parseFloat(material.get('width'));
    }
    if (type === TYPE_2) {
      quantity = perimeter / parseFloat(material.get('width'));
    }
    if (type === TYPE_3) {
      quantity = perimeter / parseFloat(material.get('width'));
    }
    if (type === TYPE_4) {
      quantity = (variantInstallation === VARIANT_INSTALLATION_2) ? perimeter * 9.5 : perimeter * 6;
    }
    if (type === TYPE_5) {
      quantity = perimeter / parseFloat(material.get('width'));
    }
    if (type === TYPE_6) {
      quantity = (variantInstallation === VARIANT_INSTALLATION_2) ? perimeter * 9.5 : perimeter * 6;
    }
    if (type === TYPE_7) {
      quantity = (variantInstallation === VARIANT_INSTALLATION_2) ? perimeter * 0.06 : perimeter * 0.04;
    }
    if (type === TYPE_8) {
      quantity = perimeter * 0.06;
    }
    if (type === TYPE_7 || type === TYPE_8) {
      quantity = quantity;
    }else {
      quantity = Math.ceil(quantity);
    }

    return computeEntity(material, quantity);
  },
);

/**
 * количество саморезов
 */
const calculateScrew = () => createSelector(
  makeSelectCurrentSide(),
  makeSelectMaterialsDomain(),
  calculateMaterialBase(),
  makeSelectCountPost(),
  makeSelectPerimeter(),
  (currentSide, materials, materialBase, countPosts, perimeter) => {
    const type = currentSide.get('type'); // тип забора
    const variantInstallation = currentSide.get('variantInstallation'); // вариант установки
    const fixings = materials
      .get(TYPE_FIXINGS, fromJS([]))
      .filter(item => item.get('typeIds').includes(type));

    const entity = fixings.isEmpty() ? fromJS({}) : fixings.first();
    const unit_step = parseFloat(entity.get('unit_step', 1));
    const quantityMaterialItems = materialBase.get('quantity');
    let quantity = 0; // количество крепежа

    if (type === TYPE_1) {
      quantity = countPosts * 4;
    }
    if (type === TYPE_2) {
      quantity = countPosts * 4;
    }
    if (type === TYPE_3) {
      quantity = quantityMaterialItems * 10;
    }
    if (type === TYPE_4) {
      quantity = quantityMaterialItems * 4;
    }
    if (type === TYPE_5) {
      quantity = quantityMaterialItems * 10;
    }
    if (type === TYPE_6) {
      quantity = quantityMaterialItems * 4;
    }
    if (type === TYPE_7) {
      quantity = (variantInstallation === VARIANT_INSTALLATION_2) ? perimeter * 15 : perimeter * 10;
    }
    if (type === TYPE_8) {
      quantity = (variantInstallation === VARIANT_INSTALLATION_2) ? perimeter * 15 : perimeter * 10;
    }

    quantity = Math.ceil(Math.ceil(quantity) / unit_step) * unit_step;

    return computeEntity(entity, quantity);
  },
);

/**
 * тип используемых столбов и их стоимость
 */
const calculatePost = () => createSelector(
  makeSelectCurrentSidePost(),
  makeSelectCountPost(),
  (post, quantity) => computeEntity(post, quantity),
);

/**
 * кол-во лаг
 */
const calculateMaterialLaga = () => createSelector(
  makeSelectCurrentSide(),
  makeSelectMaterialsLaga(),
  makeSelectPerimeter(),
  (currentSide, laga, perimeter) => {
    const type = currentSide.get('type'); // тип забор
    if (type === TYPE_1 || type === TYPE_2) {
      return fromJS({});
    }
    let quantity = (perimeter / 3) * 2;
    quantity = Math.ceil(quantity);
    return computeEntity(laga, quantity);
  },
);

/**
 * подсчет кол-ва щебня
 */
const calculateBallast = () => createSelector(
  makeSelectCurrentSide(),
  makeSelectMaterialsDomain(),
  makeSelectCountPost(),
  (currentSide, materials, countPost) => {
    const type = currentSide.get('type'); // тип забора
    const installationMethod = currentSide.get('installationMethod');
    const entity = materials.get(TYPE_STONE);

    let coefficientBallast;
    if (type === TYPE_1) {
      switch (installationMethod) {
        case 'Полное бетонирование':
          coefficientBallast = 0.3;
          break;
        case 'Бутование щебнем':
          coefficientBallast = 0.5;
          break;
        case 'Частичное бетонирование':
          coefficientBallast = 0.2;
          break;
        default:
          break;
      }
    } else {
      switch (installationMethod) {
        case 'Полное бетонирование':
        case 'Бутование щебнем':
          coefficientBallast = 0.5;
          break;
        case 'Частичное бетонирование':
          coefficientBallast = 0.25;
          break;
        default:
          break;
      }
    }

    const quantity = Math.ceil(countPost * coefficientBallast); // кол-во мешков щебня, округляем в большую сторону

    return computeEntity(entity, quantity);
  },
);


/**
 * подсчет кол-ва песка
 */
const calculateSand = () => createSelector(
  makeSelectCurrentSide(),
  makeSelectMaterialsDomain(),
  makeSelectCountPost(),
  (currentSide, materials, countPost) => {
    const type = currentSide.get('type'); // тип забора
    const installationMethod = currentSide.get('installationMethod');
    const entity = materials.get(TYPE_SAND);

    let coefficient;
    if (type === TYPE_1) {
      switch (installationMethod) {
        case 'Полное бетонирование':
          coefficient = 0.3;
          break;
        case 'Частичное бетонирование':
          coefficient = 0.2;
          break;
        default:
          break;
      }
    } else {
      switch (installationMethod) {
        case 'Полное бетонирование':
          coefficient = 0.5;
          break;
        case 'Частичное бетонирование':
          coefficient = 0.25;
          break;
        default:
          break;
      }
    }

    const quantity = Math.ceil(countPost * coefficient); // кол-во мешков песка, округляем в большую сторону

    return computeEntity(entity, quantity);
  },
);


/**
 * подсчет кол-ва цемента
 */
const calculateCement = () => createSelector(
  makeSelectCurrentSide(),
  makeSelectMaterialsDomain(),
  makeSelectCountPost(),
  (currentSide, materials, countPost) => {
    const type = currentSide.get('type'); // тип забора
    const installationMethod = currentSide.get('installationMethod');
    const entity = materials.get(TYPE_CEMENT);

    let coefficient;
    if (type === TYPE_1) {
      switch (installationMethod) {
        case 'Полное бетонирование':
          coefficient = 0.1;
          break;
        case 'Частичное бетонирование':
          coefficient = 0.08;
          break;
        default:
          break;
      }
    } else {
      switch (installationMethod) {
        case 'Полное бетонирование':
          coefficient = 0.15;
          break;
        case 'Частичное бетонирование':
          coefficient = 0.1;
          break;
        default:
          break;
      }
    }

    const quantity = Math.ceil(countPost * coefficient); // кол-во мешков цемента, округляем в большую сторону

    return computeEntity(entity, quantity);
  },
);

/**
 * подсчет кол-ва ворот
 */
const calculateGate = () => createSelector(
  makeSelectCurrentSide(),
  makeSelectCurrentSideGate(),
  (currentSide, gate) => {
    return computeEntity(gate, currentSide.get('gateCount'));
  },
);


/**
 * подсчет кол-ва калиток
 */
const calculateDoor = () => createSelector(
  makeSelectCurrentSide(),
  makeSelectCurrentSideDoor(),
  (currentSide, door) => computeEntity(door, currentSide.get('doorCount')),
);

/**
 * подсчет кол-ва замков
 */
const calculateLock = () => createSelector(
  makeSelectCurrentSide(),
  makeSelectCurrentSideLock(),
  (currentSide, lock) => computeEntity(lock, currentSide.get('doorCount')),
);


const makeSelectCalculateMaterials = () => createSelector(
  calculateMaterialBase(),
  calculateScrew(),
  calculatePost(),
  calculateMaterialLaga(),
  calculateBallast(),
  calculateSand(),
  calculateCement(),
  calculateGate(),
  calculateDoor(),
  calculateLock(),
  (materialBase, screw, post, laga, ballast, sand, cement, gate, door, lock) => {
    return computeResult({
      keySum: 'priceMaterialsSum',
      keyEntities: 'materials',
      entities: [
        screw,
        materialBase,
        post,
        laga,
        ballast,
        sand,
        cement,
        gate,
        door,
        lock,
      ],
    });
  },
);

export {
  makeSelectCalculateMaterials,
};

import { fromJS } from 'immutable';

export const computeEntity = (entity, quantity) => {
  if (quantity && !entity.isEmpty()) {
    const price = entity.get('price', 0);
    const sum = parseFloat(price) * quantity;

    return fromJS({
      name: entity.get('label', entity.get('name')),
      unit: entity.get('unit'),
      quantity,
      price,
      sum,
    });
  }
  return fromJS({});
};

export const computeResult = (params) => {
  let sum = 0;
  let entities = [];
  for ( let i = 0; i < params.entities.length; i++ ) {
    const entity = params.entities[i];
    if (!entity.isEmpty()) {
      sum = sum + entity.get('sum');
      entities.push(entity);
    }
  }

  return fromJS({
    [params.keySum]: sum,
    [params.keyEntities]: entities,
  });
};

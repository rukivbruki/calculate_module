import { createSelector } from 'reselect';
import { makeSelectCurrentSide } from '../side/selectors';

const makeSelectPerimeter = () => createSelector(
  makeSelectCurrentSide(),
  (currentSide) => {
    let perimetr = parseFloat(currentSide.get('width', 0));

    const gateCount = (parseFloat(currentSide.get('gateCount', 0)) > 0) ? parseFloat(currentSide.get('gateCount', 0)) : false;
    let gateWidth = (parseFloat(currentSide.get('gateWidth', 0)) > 0) ? parseFloat(currentSide.get('gateWidth', 0)) : false;
    gateWidth = gateCount && gateWidth * gateCount;

    perimetr = gateWidth ? perimetr - gateWidth : perimetr;

    const doorCount = (parseFloat(currentSide.get('doorCount', 0) > 0)) ? parseFloat(currentSide.get('doorCount', 0)) : false;

    perimetr = doorCount ? perimetr - doorCount : perimetr;

    return perimetr;
  },
);

export {
  makeSelectPerimeter,
};

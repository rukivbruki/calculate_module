import { fromJS } from 'immutable';
import { createSelector } from 'reselect';
import { makeSelectCurrentSide } from '../side/selectors';
import {
  makeSelectCurrentTypeWorksInstallationBase,
  makeSelectCurrentTypeWorksInstallationGate,
  makeSelectCurrentTypeWorksInstallationDoor,
} from '../types/selectorWorks';
import { makeSelectPerimeter } from './perimeter';
import {
  computeEntity,
  computeResult,
} from './helpers';
import {
  makeSelectBase,
  makeSelectCountPost,
} from './base';
import {
  makeSelectCurrentSideGate,
  makeSelectCurrentSideDoor,
} from '../types/selectors';

/**
 * расчитывает кол-во единиц измерения работы по изготовлению фундамента
 * возвращает или кол-во столбов или длинну периметра
 */
const makeSelectUnitWork = () => createSelector(
  makeSelectBase(),
  makeSelectPerimeter(),
  makeSelectCountPost(),
  (work, perimeter, countPost) => {
    if (work.get('unit') === 'м.п.') {
      return perimeter;
    }
    if (work.get('unit') === 'шт.') {
      return countPost;
    }
    return null;
  },
);

/**
 * подсчет объема работ по установке столбов или заливке фундамента
 */
const makeSelectBaseWork = () => createSelector(
  makeSelectBase(),
  makeSelectUnitWork(),
  (entity, quantity) => {
    if (quantity) {
      return computeEntity(entity, quantity);
    }
    return fromJS({});
  },
);

/**
 * работы по монтажу забора на основание
 * CATEGORY_2
 */
const makeSelectInstallationMaterial = () => createSelector(
  makeSelectCurrentTypeWorksInstallationBase(),
  makeSelectPerimeter(),
  (entity, perimeter) => {
    if (perimeter) {
      return computeEntity(entity, perimeter);
    }
    return fromJS({});
  },
);

/**
 * работы по монтажу ворот
 */
const makeSelectInstallationGate = () => createSelector(
  makeSelectCurrentSide(),
  makeSelectCurrentTypeWorksInstallationGate(),
  makeSelectCurrentSideGate(),
  (currentSide, entity, gate) => {
    const quantity = currentSide.get('gateCount');
    if (quantity && !gate.isEmpty()) {
      return computeEntity(entity, quantity);
    }
    return fromJS({});
  },
);


/**
 * работы по монтажу калиток
 * CATEGORY_5
 */
const makeSelectInstallationDoor = () => createSelector(
  makeSelectCurrentSide(),
  makeSelectCurrentTypeWorksInstallationDoor(),
  makeSelectCurrentSideDoor(),
  (currentSide, entity, door) => {
    const quantity = currentSide.get('doorCount');
    if (quantity && !door.isEmpty()) {
      return computeEntity(entity, quantity);
    }
    return fromJS({});
  },
);


const calculateWork = () => createSelector(
  makeSelectBaseWork(),
  makeSelectInstallationMaterial(),
  makeSelectInstallationGate(),
  makeSelectInstallationDoor(),
  (baseWork, installationMaterial, installationGate, installationDoor) => {
    return computeResult({
      keySum: 'priceWorksSum',
      keyEntities: 'works',
      entities: [
        baseWork,
        installationMaterial,
        installationGate,
        installationDoor,
      ],
    });
  },
);


export {
  calculateWork,
};

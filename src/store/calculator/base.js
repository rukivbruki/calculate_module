import { fromJS } from 'immutable';
import { createSelector } from 'reselect';
import { makeSelectCurrentSide } from '../side/selectors';
import { makeSelectCurrentTypeInstallationMethods } from '../types/selectors';
import { makeSelectPerimeter } from './perimeter';


/**
 * тип фундамента
 */
const makeSelectBase = () => createSelector(
  makeSelectCurrentSide(),
  makeSelectCurrentTypeInstallationMethods(),
  (currentSide, installationMethods) => {
    const installationMethod = installationMethods.filter(item => item.get('name') === currentSide.get('installationMethod'));

    if (installationMethod.isEmpty()) {
      return fromJS({});
    }
    return installationMethod.first();
  },
);

/**
 * возвращает кол-во столбов
 */
const makeSelectCountPost = () => createSelector(
  makeSelectPerimeter(),
  (perimeter) => {
    return perimeter ? Math.ceil(perimeter / 2.5) + 1 : 0;
  },
);


export {
  makeSelectBase,
  makeSelectCountPost,
};

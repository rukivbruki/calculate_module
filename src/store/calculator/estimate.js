import { fromJS } from 'immutable';
import { createSelector } from 'reselect';
import { calculateWork } from './work';
import { makeSelectCalculateMaterials } from './materials';

const makeSelectEstimate = () => createSelector(
  calculateWork(),
  makeSelectCalculateMaterials(),
  (work, materials) => {
    const returnMap = fromJS({});
    return returnMap
      .mergeDeep(work)
      .mergeDeep(materials);
  },
);

export {
  makeSelectEstimate,
};

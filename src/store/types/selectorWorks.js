import { fromJS } from 'immutable';
import { createSelector } from 'reselect';
import { makeSelectCurrentType } from './selectors';
import { makeSelectCurrentSide } from '../side/selectors';


const makeSelectCurrentTypeWorks = () => createSelector(
  makeSelectCurrentType(),
  (currentType) => {
    return currentType.get('works', fromJS({}));
  },
);

const makeSelectCurrentTypeWorksInstallationBase = () => createSelector(
  makeSelectCurrentTypeWorks(),
  makeSelectCurrentSide(),
  (works, side) => {
    const installationBase = works.get('installationBase', fromJS({}));
    const variantInstallation = side.get('variantInstallation');
    if(variantInstallation){
      return installationBase.get(variantInstallation, fromJS({}));
    }
    return installationBase;
  },
);

const makeSelectCurrentTypeWorksInstallationGate = () => createSelector(
  makeSelectCurrentTypeWorks(),
  (works) => works.get('installationGate', fromJS({})),
);

const makeSelectCurrentTypeWorksInstallationDoor = () => createSelector(
  makeSelectCurrentTypeWorks(),
  (works) => works.get('installationDoor', fromJS({})),
);

export {
  makeSelectCurrentTypeWorksInstallationBase,
  makeSelectCurrentTypeWorksInstallationGate,
  makeSelectCurrentTypeWorksInstallationDoor,
};

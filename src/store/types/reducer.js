import { fromJS } from 'immutable';
import types from '../../data/types';

const initialState = fromJS(types);

function typeReducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}

export default typeReducer;

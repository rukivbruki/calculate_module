import { fromJS } from 'immutable';
import { createSelector } from 'reselect';
import { makeSelectCurrentSide } from '../side/selectors';

const makeSelectTypes = () => state => state.get('types');

const makeSelectCurrentType = () => createSelector(
  makeSelectTypes(),
  makeSelectCurrentSide(),
  (types, currentSide) => {
    return types.get(currentSide.get('type'), fromJS({}));
  },
);

const makeSelectCurrentTypeMaterials = () => createSelector(
  makeSelectCurrentType(),
  makeSelectCurrentSide(),
  (currentType, currentSide) => {
    return currentType
      .get('materials', fromJS([]))
      .filter(item => item.get('height') === currentSide.get('height'));
  },
);

const makeSelectCurrentTypeVariantInstallation = () => createSelector(
  makeSelectCurrentType(),
  (currentType) => {
    return currentType.get('variantInstallation', fromJS([]));
  },
);

const makeSelectCurrentTypeInstallationMethods = () => createSelector(
  makeSelectCurrentType(),
  (currentType) => {
    return currentType.get('installationMethods', fromJS([]));
  },
);

const makeSelectCurrentTypeGateTypes = () => createSelector(
  makeSelectCurrentType(),
  makeSelectCurrentSide(),
  (currentType, currentSide) => {
    return currentType
      .get('gates', fromJS([]))
      .filter(item => item.get('showTypes'))
      .filter(item => item.get('height') === currentSide.get('height'));
  },
);

const makeSelectCurrentTypeGateWidth = () => createSelector(
  makeSelectCurrentType(),
  makeSelectCurrentSide(),
  (currentType, currentSide) => {
    const currentGateType = currentType
      .get('gates', fromJS([]))
      .filter(item => item.get('showTypes'))
      .filter(item => item.get('name') === currentSide.get('gateType'))
      .filter(item => item.get('height') === currentSide.get('height'))
      .first();
    if (currentGateType) {
      return currentGateType.get('variantWidth', fromJS([]));
    }
    return fromJS([]);
  },
);

const makeSelectCurrentTypeDoors = () => createSelector(
  makeSelectCurrentType(),
  makeSelectCurrentSide(),
  (currentType, currentSide) => {
    return currentType
      .get('doors', fromJS([]))
      .filter(item => item.get('height') === currentSide.get('height'));
  },
);

const makeSelectCurrentTypeLocks = () => createSelector(
  makeSelectCurrentType(),
  makeSelectCurrentSide(),
  (currentType) => {
    return currentType.get('locks', fromJS([]));
  },
);

const makeSelectCurrentSideMaterial = () => createSelector(
  makeSelectCurrentTypeMaterials(),
  makeSelectCurrentSide(),
  (currentTypeMaterials, currentSide) => {
    const entity = currentTypeMaterials
      .filter(item => item.get('name') === currentSide.get('material'));

    if (entity.isEmpty()) {
      return fromJS({});
    }
    return entity.first();
  },
);

const makeSelectCurrentSideGate = () => createSelector(
  makeSelectCurrentType(),
  makeSelectCurrentSide(),
  (currentType, currentSide) => {
    let entity;
    if (currentSide.get('gateName')) {
      entity = currentType
        .get('gates', fromJS([]))
        .filter(item => item.get('name') === currentSide.get('gateName'));
    } else if (currentSide.get('gateType') && currentSide.get('height') && currentSide.get('gateWidth')) {
      entity = currentType
        .get('gates', fromJS([]))
        .filter(item => item.get('type') === currentSide.get('gateType'))
        .filter(item => item.get('height') === currentSide.get('height'))
        .filter(item => item.get('width') === currentSide.get('gateWidth'));
    } else {
      entity = fromJS({});
    }

    if (entity.isEmpty()) {
      return fromJS({});
    }
    return entity.first();
  },
);

const makeSelectCurrentSideDoor = () => createSelector(
  makeSelectCurrentType(),
  makeSelectCurrentSide(),
  (currentType, currentSide) => {
    const entity = currentType
      .get('doors', fromJS([]))
      .filter(item => item.get('name') === currentSide.get('doorName'));

    if (entity.isEmpty()) {
      return fromJS({});
    }
    return entity.first();
  },
);

const makeSelectCurrentSideLock = () => createSelector(
  makeSelectCurrentType(),
  makeSelectCurrentSide(),
  (currentType, currentSide) => {
    const entity = currentType
      .get('locks', fromJS([]))
      .filter(item => item.get('name') === currentSide.get('lockName'));

    if (entity.isEmpty()) {
      return fromJS({});
    }
    return entity.first();
  },
);

export {
  makeSelectTypes,
  makeSelectCurrentType,
  makeSelectCurrentTypeMaterials,
  makeSelectCurrentTypeVariantInstallation,
  makeSelectCurrentTypeInstallationMethods,
  makeSelectCurrentTypeGateTypes,
  makeSelectCurrentTypeGateWidth,
  makeSelectCurrentTypeDoors,
  makeSelectCurrentTypeLocks,

  makeSelectCurrentSideMaterial,
  makeSelectCurrentSideGate,
  makeSelectCurrentSideDoor,
  makeSelectCurrentSideLock,
};

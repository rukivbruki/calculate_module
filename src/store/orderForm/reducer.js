import { fromJS } from 'immutable';
import { ORDER_LOAD } from './constants';

const orderFormData = () => {
  const name = localStorage.getItem('inputName');
  const email = localStorage.getItem('inputEmail');
  const phone = localStorage.getItem('inputPhone');

  const user = fromJS({
    rule: true,
    name: name ? name : null,
    email: email ? email : null,
    phone: phone ? phone : null,
  });

  return fromJS({
    user: user,
    order: {
      form: 'calculator',
      asset: null,
      rule: true,
    },
  });
};

function orderFormReducer(state = orderFormData(), action) {
  switch (action.type) {
    case ORDER_LOAD:
      return state.mergeDeep(action.payload);
    default:
      return state;
  }
}

export default orderFormReducer;

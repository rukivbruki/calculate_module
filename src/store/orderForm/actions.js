import { ORDER_LOAD } from './constants';

export function setData(payload) {
  return (dispatch, getState) => {
    dispatch({
      type: ORDER_LOAD,
      payload,
    });
  };
}

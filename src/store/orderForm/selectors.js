import { createSelector } from 'reselect';

const makeSelectOrderForm = state => state.get('orderForm');

export {
  makeSelectOrderForm,
};

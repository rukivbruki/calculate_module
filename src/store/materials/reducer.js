import { fromJS } from 'immutable';
import materials from '../../data/materials';

function materialsReducer(state = fromJS(materials), action) {
  switch (action.type) {
    default:
      return state;
  }
}

export default materialsReducer;

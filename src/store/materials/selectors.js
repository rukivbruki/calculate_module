import { fromJS } from 'immutable';
import { createSelector } from 'reselect';
import { makeSelectCurrentSide } from '../side/selectors';
import {
  CATEGORY_POST,
  TYPE_LAGA,
} from '../../data/constants';

const makeSelectMaterialsDomain = () => state => state.get('materials');

const makeSelectMaterialsPosts = () => createSelector(
  makeSelectCurrentSide(),
  makeSelectMaterialsDomain(),
  (currentSide, materials) => {
    return materials
      .filter(item => item.get('category') === CATEGORY_POST)
      .filter(item => item.get('typeIds').includes(currentSide.get('type')));
  },
);

const makeSelectCurrentSidePost = () => createSelector(
  makeSelectMaterialsDomain(),
  makeSelectCurrentSide(),
  (materials, currentSide) => materials.get(currentSide.get('post'), fromJS({})),
);

const makeSelectMaterialsLaga = () => createSelector(
  makeSelectMaterialsDomain(),
  (materials) => materials.get(TYPE_LAGA, fromJS({})),
);

const makeSelectColors = () => createSelector(
  makeSelectMaterialsDomain(),
  (materials) => materials.get('colors', fromJS([])),
);

export {
  makeSelectMaterialsDomain,
  makeSelectMaterialsPosts,
  makeSelectCurrentSidePost,
  makeSelectMaterialsLaga,
  makeSelectColors,
};

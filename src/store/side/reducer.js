import { fromJS } from 'immutable';
import {
  ADD_SIDE,
  REMOVE_SIDE,
  SELECT_SIDE,
  UPDATE_SIDES,
  RESET_SIDES,
  SET_COLOR,
} from './constants';

let initialState;
const sideData = JSON.parse(localStorage.getItem('side'));

const deafaultSide = fromJS({
  currentSideId: '1',
  color: {
    label: 'Выберите цвет',
  },
  sides: {
    '1': {
      id: '1',
      label: 'Сторона 1',
      editing: false,
    },
  },
});

if (sideData) {
  initialState = fromJS(sideData);
} else {
  initialState = deafaultSide;
}

function sideReducer(state = initialState, action) {
  switch (action.type) {
    case ADD_SIDE:
      return state
        .setIn([
          'sides',
          action.payload.sideId,
        ], fromJS(action.payload.params))
        .set('currentSideId', action.payload.sideId);
    case SET_COLOR:
      return state.set('color', fromJS(action.payload));
    case REMOVE_SIDE:
      return state
        .set('sides', state.get('sides').remove(action.payload))
        .set('currentSideId', state.get('sides').remove(action.payload).keySeq().last());
    case SELECT_SIDE:
      return state
        .set('currentSideId', action.payload);
    case UPDATE_SIDES:
      return state
        .setIn([
          'sides',
          action.payload.get('currentSideId'),
        ], action.payload.get('data'));
    case RESET_SIDES:
      return deafaultSide;
    default:
      return state;
  }
}

export default sideReducer;

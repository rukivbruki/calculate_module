import { createSelector } from 'reselect';

const makeSelectSideDomain = state => state.get('side');

const makeSelectCurrentSideId = () => createSelector(
  makeSelectSideDomain,
  state => state.get('currentSideId'),
);

const makeSelectSides = () => createSelector(
  makeSelectSideDomain,
  (state) => {
    return state.get('sides');
  },
);

const makeSelectCurrentSide = () => createSelector(
  makeSelectSides(),
  makeSelectCurrentSideId(),
  (sides, currentSideId) => sides.get(currentSideId),
);

const makeSelectSum = () => createSelector(
  makeSelectSides(),
  (sides) => {
    const sidesObj = sides.toJS();
    const sidesKeys = Object.keys(sidesObj);
    let sum = 0;
    for ( let i = 0; i < sidesKeys.length; i++ ) {
      const priceMaterialsSum = sides.getIn([
        sidesKeys[i],
        'estimate',
        'priceMaterialsSum',
      ]);
      const priceWorksSum = sides.getIn([
        sidesKeys[i],
        'estimate',
        'priceWorksSum',
      ]);
      sum = priceMaterialsSum ? sum + priceMaterialsSum : sum;
      sum = priceWorksSum ? sum + priceWorksSum : sum;
    }
    return sum;
  },
);

const makeSelectColor = () => createSelector(
  makeSelectSideDomain,
  (state) => {
    return state.get('color');
  },
);

export {
  makeSelectSideDomain,
  makeSelectCurrentSide,
  makeSelectSides,
  makeSelectSum,
  makeSelectColor,
};

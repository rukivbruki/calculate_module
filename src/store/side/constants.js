export const ADD_SIDE = 'app/ADD_SIDE';
export const REMOVE_SIDE = 'app/REMOVE_SIDE';
export const SELECT_SIDE = 'app/SELECT_SIDE';
export const UPDATE_SIDES = 'app/UPDATE_SIDES';
export const RESET_SIDES = 'app/RESET_SIDES';
export const SET_COLOR = 'app/SET_COLOR';

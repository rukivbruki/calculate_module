import {
  ADD_SIDE,
  REMOVE_SIDE,
  SELECT_SIDE,
  UPDATE_SIDES,
  RESET_SIDES,
  SET_COLOR,
} from './constants';

export function addSide() {
  return (dispatch, getState) => {
    const state = getState();
    const sides = state.getIn([
      'side',
      'sides',
    ]);
    const lastSideId = parseInt(sides.keySeq().last(), 10) ? parseInt(sides.keySeq().last(), 10) : 0;
    const sideId = `${lastSideId + 1}`;
    const params = { id: sideId, label: `Сторона ${sideId}` };

    window.scrollTo(0, 0);

    dispatch({
      type: ADD_SIDE,
      payload: { sideId, params },
    });
  };
}

export function removeSide(sideId) {
  window.scrollTo(0, 0);
  return (dispatch, getState) => {
    const state = getState();
    const sides = state.getIn([
      'side',
      'sides',
    ]);

    if (sides.size === 1) {
      dispatch({
        type: RESET_SIDES,
      });
    } else {
      dispatch({
        type: REMOVE_SIDE,
        payload: sideId,
      });
    }
  };
}

export function selectSide(params) {
  window.scrollTo(0, 0);
  return (dispatch) => {
    dispatch({
      type: SELECT_SIDE,
      payload: params,
    });
  };
}

export function setColor(payload) {
  return (dispatch) => {
    dispatch({
      type: SET_COLOR,
      payload,
    });
  };
}

export function updateSides(payloadData) {
  let payload;
  const currentSideId = payloadData.get('currentSideId', null);
  const isLabel = payloadData.getIn([
    'data',
    'label',
  ], null);

  return (dispatch, getState) => {
    const state = getState();
    const oldLabel = state.getIn([
      'side',
      'sides',
      currentSideId,
      'label',
    ]);
    if (!isLabel) {
      payload = payloadData.setIn([
        'data',
        'label',
      ], oldLabel);
    } else {
      payload = payloadData;
    }
    dispatch({
      type: UPDATE_SIDES,
      payload,
    });
  };
}

export function resetSides() {
  return (dispatch) => {
    dispatch({
      type: RESET_SIDES,
    });
  };
}

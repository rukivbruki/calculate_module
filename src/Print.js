import React, { Component } from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'recompose';

import { makeSelectSides } from './store/side/selectors';


class Print extends Component {

  calculateSum = () => {
    return this.props.sides.reduce(function(sum, sideItem) {
      return sum + parseFloat(sideItem.getIn([
            'estimate',
            'sideSum',
          ]));
    }, 0);
  };

  render() {
    return (
      <div className="estimate-table">
        {!this.props.sides.isEmpty() && this.props.sides.entrySeq().map(([key, sideItem]) =>
          <div key={key}>
            <h2>
              {sideItem.get('label')}
            </h2>
            <div>
              <h5>Работы:</h5>
              <table style={{ fontSize: 16 }}>
                <thead>
                <tr>
                  <th>Наименование работ</th>
                  <th>ед.изм.</th>
                  <th>Кол-во</th>
                  <th>Цена за ед.</th>
                  <th>Сумма</th>
                </tr>
                </thead>
                <tbody>
                {sideItem.getIn([
                  'estimate',
                  'works',
                ], null) && sideItem.getIn([
                  'estimate',
                  'works',
                ]).map((item) => {
                  return (
                    <tr key={item.get('name')}>
                      <td>{item.get('name')}</td>
                      <td>{item.get('unit')}</td>
                      <td>{item.get('quantity')}</td>
                      <td>{item.get('price')}</td>
                      <td>{item.get('sum')}</td>
                    </tr>
                  );
                })}
                <tr>
                  <td colSpan="4" align="right">Итого по работе:</td>
                  <td>
                    {sideItem.getIn([
                      'estimate',
                      'priceWorksSum',
                    ])} руб.
                  </td>
                </tr>
                </tbody>
              </table>

              <h5>Материалы на объём работ</h5>
              <table style={{ fontSize: 16 }}>
                <thead>
                <tr>
                  <th>Наименование материала</th>
                  <th>ед.изм.</th>
                  <th>Кол-во</th>
                  <th>Цена за ед.</th>
                  <th>Сумма</th>
                </tr>
                </thead>
                <tbody>
                {sideItem.getIn([
                  'estimate',
                  'materials',
                ], null) && sideItem.getIn([
                  'estimate',
                  'materials',
                ]).map((item) => {
                  return (
                    <tr key={item.get('name')}>
                      <td>{item.get('name')}</td>
                      <td>{item.get('unit')}</td>
                      <td>{item.get('quantity')}</td>
                      <td>{item.get('price')}</td>
                      <td>{item.get('sum')}</td>
                    </tr>
                  );
                })}
                <tr>
                  <td colSpan="4" align="right">Итого по материалам:</td>
                  <td>
                    {sideItem.getIn([
                      'estimate',
                      'priceMaterialsSum',
                    ])} руб.
                  </td>
                </tr>
                </tbody>
              </table>

              <div className="sideSum">
                <strong>
                  Итого по стороне:
                  {sideItem.getIn([
                    'estimate',
                    'sideSum',
                  ])} руб.
                </strong>
              </div>
            </div>
          </div>,
        )}
        <div className="sideSum">
          <strong>
            Итого:
            {this.calculateSum()} руб.
          </strong>
        </div>
        <p>
          ЛЕНЗАБОР.РУ - установка и изготовление заборов в Санкт-Петербурге и Ленинградской области.
        </p>
        <p>
          Россия, г. Кировск, Ленинградская область, ул. Песочная, д. 6
          35 минут езды от КАД
        </p>
        <p>
          Время работы офиса
          <br />
          Пн-Пт с 09:00 до 18:00
          <br />
          Сб с 10:00 до 13:00 (по предварительной договорённости)
        </p>
        <p>
          Наши телефоны:
          <br />
          (812) 337-15-64
          <br />
          8 (911) 175-42-71
        </p>
        <p>
          e-mail: info@lenzabor.ru
        </p>
      </div>
    );
  }
}

Print.propTypes = {
  sides: ImmutablePropTypes.map,
};

const mapStateToProps = (state, ownProps) => {
  return createStructuredSelector({
    sides: makeSelectSides(),
  });
};


const wrapper = compose(
  connect(
    mapStateToProps,
  ),
);

export default wrapper(Print);

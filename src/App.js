import { fromJS } from 'immutable';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'recompose';

import MaterialTypes from './containers/MaterialTypes';
import CalculatorPanel from './containers/CalculatorPanel';
import SelectSide from './containers/SelectSide';
import SelectVariantHeight from './containers/SelectVariantHeight';
import SelectMaterials from './containers/SelectMaterials';
import SelectVariantInstallation from './containers/SelectVariantInstallation';
import SelectMaterialPost from './containers/SelectMaterialPost';
import SelectInstallationMethod from './containers/SelectInstallationMethod';
import SelectWidth from './containers/SelectWidth';
import SelectCountGate from './containers/SelectCountGate';
import SelectGateType from './containers/SelectGateType';
import SelectGateWidth from './containers/SelectGateWidth';
import SelectCountDoor from './containers/SelectCountDoor';
import SelectDoor from './containers/SelectDoor';
import SelectLock from './containers/SelectLock';

import { updateSides } from './store/side/actions';
import { makeSelectCurrentSide } from './store/side/selectors';
import {
  makeSelectTypes,
  makeSelectCurrentType,
  makeSelectCurrentTypeMaterials,
  makeSelectCurrentTypeVariantInstallation,
  makeSelectCurrentTypeInstallationMethods,
  makeSelectCurrentTypeGateTypes,
  makeSelectCurrentTypeGateWidth,
  makeSelectCurrentTypeDoors,
  makeSelectCurrentTypeLocks,
} from './store/types/selectors';
import {
  makeSelectMaterialsPosts,
} from './store/materials/selectors';
import {
  makeSelectEstimate,
} from './store/calculator/estimate';


class App extends Component {

  componentWillReceiveProps(newProps) {
    const { currentSide } = newProps;
    const priceWorksSum = currentSide.getIn([
      'estimate',
      'priceWorksSum',
    ]);
    const priceMaterialsSum = currentSide.getIn([
      'estimate',
      'priceMaterialsSum',
    ]);

    if (priceWorksSum !== newProps.estimate.get('priceWorksSum') ||
        priceMaterialsSum !== newProps.estimate.get('priceMaterialsSum')) {
      const sideSum = newProps.estimate.get('priceWorksSum') + newProps.estimate.get('priceMaterialsSum');
      const currentSideData = currentSide
        .set('estimate', newProps.estimate)
        .setIn(['estimate', 'sideSum'], sideSum);
      const payload = fromJS({
        currentSideId: currentSide.get('id'),
        data: currentSideData,
      });
      this.props.updateSides(payload);
    }
  }

  handleUpdateSide = (data, resetTree) => {
    const { currentSide } = this.props;
    const currentSideData = resetTree ? fromJS(data) : currentSide.mergeDeep(fromJS(data));
    const payload = fromJS({
      currentSideId: currentSide.get('id'),
      data: currentSideData,
    });
    this.props.updateSides(payload);
  };

  render() {
    const {
      currentType,
      currentSide,
      estimate,
    } = this.props;

    return (
      <div className="section-calculator">
        <div className="section-calculator__grid clearfix">

          <div className="section-calculator__content">
            <h1 className="heading heading_line section-calculator__heading">Калькулятор расчета стоимости забора </h1>
            <p className="section-calculator__text section-calculator__text_offset-left">Стороны забора </p>

            <SelectSide />

            <MaterialTypes
              items={this.props.types}
              currentSide={currentSide}
              updateSides={this.handleUpdateSide}
            />

            <SelectVariantHeight
              currentType={currentType}
              currentSide={currentSide}
              updateSides={this.handleUpdateSide}
            />

            <SelectMaterials
              items={this.props.materials}
              currentSide={currentSide}
              updateSides={this.handleUpdateSide}
            />

            <SelectVariantInstallation
              items={this.props.variantInstallation}
              currentSide={currentSide}
              updateSides={this.handleUpdateSide}
            />

            <SelectMaterialPost
              items={this.props.posts}
              currentSide={currentSide}
              updateSides={this.handleUpdateSide}
            />

            <SelectInstallationMethod
              items={this.props.installationMethods}
              currentSide={currentSide}
              updateSides={this.handleUpdateSide}
            />

            <SelectWidth
              currentSide={currentSide}
              updateSides={this.handleUpdateSide}
            />

            <SelectCountGate
              currentSide={currentSide}
              updateSides={this.handleUpdateSide}
            />

            <SelectGateType
              items={this.props.gateTypes}
              currentSide={currentSide}
              updateSides={this.handleUpdateSide}
            />

            <SelectGateWidth
              items={this.props.gateWidth}
              currentSide={currentSide}
              updateSides={this.handleUpdateSide}
            />

            <SelectCountDoor
              currentSide={currentSide}
              updateSides={this.handleUpdateSide}
            />

            <SelectDoor
              items={this.props.doors}
              currentSide={currentSide}
              updateSides={this.handleUpdateSide}
            />

            <SelectLock
              items={this.props.locks}
              currentSide={currentSide}
              updateSides={this.handleUpdateSide}
            />

            {(process.env.NODE_ENV === 'development') &&
             <div className="estimate-table">
               <h5>Работы:</h5>
               <table style={{ fontSize: 16 }}>
                 <thead>
                 <tr>
                   <th>Наименование работ</th>
                   <th>ед.изм.</th>
                   <th>Кол-во</th>
                   <th>Цена за ед.</th>
                   <th>Сумма</th>
                 </tr>
                 </thead>
                 <tbody>
                 {estimate.get('works').map((item) => {
                   return (
                     <tr key={item.get('name')}>
                       <td>{item.get('name')}</td>
                       <td>{item.get('unit')}</td>
                       <td>{item.get('quantity')}</td>
                       <td>{item.get('price')}</td>
                       <td>{item.get('sum')}</td>
                     </tr>
                   );
                 })}
                 <tr>
                   <td colSpan="4" align="right">Итого по работе:</td>
                   <td>
                     {estimate.get('priceWorksSum')} руб.
                   </td>
                 </tr>
                 </tbody>
               </table>

               <h5>Материалы на объём работ</h5>
               <table style={{ fontSize: 16 }}>
                 <thead>
                 <tr>
                   <th>Наименование материала</th>
                   <th>ед.изм.</th>
                   <th>Кол-во</th>
                   <th>Цена за ед.</th>
                   <th>Сумма</th>
                 </tr>
                 </thead>
                 <tbody>
                 {estimate.get('materials').map((item) => {
                   return (
                     <tr key={item.get('name')}>
                       <td>{item.get('name')}</td>
                       <td>{item.get('unit')}</td>
                       <td>{item.get('quantity')}</td>
                       <td>{item.get('price')}</td>
                       <td>{item.get('sum')}</td>
                     </tr>
                   );
                 })}
                 <tr>
                   <td colSpan="4" align="right">Итого по материалам:</td>
                   <td>
                     {estimate.get('priceMaterialsSum')} руб.
                   </td>
                 </tr>
                 </tbody>
               </table>
             </div>
            }

          </div>

          <CalculatorPanel />
        </div>
      </div>
    );
  }
}

App.propTypes = {
  currentSide: ImmutablePropTypes.map,
  types: ImmutablePropTypes.map,
  currentType: ImmutablePropTypes.map,
  materials: ImmutablePropTypes.list,
  variantInstallation: ImmutablePropTypes.list,
  installationMethods: ImmutablePropTypes.list,
  gateWidth: ImmutablePropTypes.list,
  gateTypes: ImmutablePropTypes.list,
  doors: ImmutablePropTypes.list,
  locks: ImmutablePropTypes.list,

  posts: ImmutablePropTypes.map,

  estimate: ImmutablePropTypes.map,

  updateSides: PropTypes.func,
};

const mapStateToProps = (state, ownProps) => {
  return createStructuredSelector({
    currentSide: makeSelectCurrentSide(),
    types: makeSelectTypes(),
    currentType: makeSelectCurrentType(),
    materials: makeSelectCurrentTypeMaterials(),
    variantInstallation: makeSelectCurrentTypeVariantInstallation(),
    installationMethods: makeSelectCurrentTypeInstallationMethods(),
    gateTypes: makeSelectCurrentTypeGateTypes(),
    gateWidth: makeSelectCurrentTypeGateWidth(),
    doors: makeSelectCurrentTypeDoors(),
    locks: makeSelectCurrentTypeLocks(),

    posts: makeSelectMaterialsPosts(),

    estimate: makeSelectEstimate(),
  });
};


const wrapper = compose(
  connect(
    mapStateToProps,
    dispatch => bindActionCreators({
      updateSides,
    }, dispatch),
  ),
);

export default wrapper(App);

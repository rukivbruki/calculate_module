import {
  applyMiddleware,
  compose,
  createStore,
} from 'redux';
import { fromJS } from 'immutable';
import thunk from 'redux-thunk';
// import {save,load} from 'redux-localstorage-simple';
import rootReducer from './store/reducers';
import storageAvailable from './helpers/storageAvailable';


// const initialState = load();

const initialState = fromJS({});

const logger = store => next => action => {
  let result = next(action);
  if (storageAvailable()) {
    const sideState = store.getState().get('side').toJS();
    const orderFormState = store.getState().getIn(['form', 'orderForm', 'values', 'user']);

    if (action.type === '@@redux-form/SET_SUBMIT_SUCCEEDED') {
      const inputName = orderFormState.get('name');
      localStorage.setItem('inputName', inputName);

      const inputEmail = orderFormState.get('email');
      localStorage.setItem('inputEmail', inputEmail);

      const inputPhone = orderFormState.get('phone');
      localStorage.setItem('inputPhone', inputPhone);
    }

    localStorage.setItem('side', JSON.stringify(sideState));
  }
  return result;
};

const enhancers = [];
const middleware = [
  thunk,
  logger,
  // save(),
];

if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.devToolsExtension;

  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension());
  }
}

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers,
);

const store = createStore(
  rootReducer,
  initialState,
  composedEnhancers,
);

export default store;

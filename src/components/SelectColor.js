import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import enhanceWithClickOutside from 'react-click-outside';
import ImmutablePropTypes from 'react-immutable-proptypes';

class SelectObject extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
    };
  }

  handleClickOutside = () => {
    this.state.isOpen && this.handleOpen();
  };

  handleOpen = () => {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  };

  handleSelect = (e, item) => {
    e.preventDefault();
    this.props.onSelected(item);
    this.handleOpen();
  };

  render() {
    const isOpen = this.state.isOpen;
    const currentItem = this.props.currentItem;

    const labelClassName = cn(
      'custom-select-react__drop-button',
      { 'custom-select-react__drop-button_active': isOpen },
    );

    const optionClassName = (option) => cn(
      'custom-select-react__drop-item',
      { 'custom-select-react__drop-item_selected': option.get('label') === currentItem },
    );

    const defaultStyle = {
      transition: `all 300ms ease-in-out`,
      maxHeight: 0,
      display: 'block',
    };

    const openStyle = isOpen ? {
      display: 'block',
      maxHeight: '300px',
    } : {};

    return (
      <div className="custom-select-react custom-select_color">
        <div
          className={labelClassName}
          onClick={this.handleOpen}
        >
          <span className="custom-select__color" style={{ background: currentItem.get('bg') }}></span>
          <span className="custom-select__text">{currentItem.get('label')}</span>
</div>
        <ul className="custom-select-react__drop-list" style={{
          ...defaultStyle,
          ...openStyle,
        }}>
          {this.props.items.entrySeq().map(([key, item]) =>
            <li
              className={optionClassName(item)} key={key}
              onClick={(e) => this.handleSelect(e, item)}
            >
              <span className="custom-select-react__drop-link">
                <span className="custom-select__color" style={{ background: item.get('bg') }}></span>
                <span className="custom-select__text">{item.get('label')}</span>
              </span>
            </li>,
          )}
        </ul>
      </div>
    );
  }
}

SelectObject.propTypes = {
  items: ImmutablePropTypes.list,
  currentItem: PropTypes.object,
  onSelected: PropTypes.func,
};


export default enhanceWithClickOutside(SelectObject);

import ImmutablePropTypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import shortid from 'shortid';
import Modal from 'react-modal';

const customStyles = {
  content: {},
};

// Make sure to bind modal to your appElement (http://reactcommunity.org/react-modal/accessibility/)
Modal.setAppElement('#root');

class MaterialItem extends PureComponent {
  constructor() {
    super();

    this.state = {
      modalIsOpen: false,
    };

    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  openModal() {
    this.setState({ modalIsOpen: true });
  }

  afterOpenModal() {
    // references are now sync'd and can be accessed.
    // this.subtitle.style.color = '#f00';
  }

  closeModal() {
    this.setState({ modalIsOpen: false });
  }

  handleChange = () => {
    this.props.onChange(this.props.data);
  };

  render() {
    const { data } = this.props;
    const key = shortid.generate();

    return (
      <div className="section-calculator__col" key={key}>
        <label
          className={`side-item side-item_calculator section-calculator__side-item ${this.props.selected && 'side-item_active'}`}
          htmlFor={`fence_type${key}`}
        >
                    <span
                      className="side-item__img"
                      style={{ background: `url(${data.get('prevImg')}) center no-repeat`, backgroundSize: 'cover' }}
                    >
                      <span
                        className="side-item__popup"
                        href={data.get('img')}
                        onClick={this.openModal}
                      />
                    </span>
          <br />
          <span
            className="radio">
                      <input
                        className="radio__input"
                        type="radio"
                        name={this.props.inputName}
                        value={data.get('type')}
                        id={`fence_type${key}`}
                        onChange={this.handleChange}
                        checked={this.props.selected}
                      />
                      <span className="radio__label">
                        <span className="radio__dot"></span>
                        <span className="radio__text">{data.get('name')}</span>
                      </span>
                    </span>
        </label>
        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Example Modal"
          className="LenzaborModal"
          overlayClassName="LenzaborModalOverlay"
        >
          <span
            className="LenzaborModalClose"
            onClick={this.closeModal}
          >×</span>
          <div className="LenzaborModalContent">
            <img src={data.get('img')} alt="" />
          </div>
        </Modal>
      </div>
    );
  }
}

MaterialItem.propTypes = {
  data: ImmutablePropTypes.map,
  inputName: PropTypes.string,
  selected: PropTypes.bool,
};

export default MaterialItem;

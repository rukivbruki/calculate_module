import PropTypes from 'prop-types';
import React from 'react';
import cn from 'classnames';


const CalculatorStep = (props) => {
  const ClassNames = cn(
    'section-calculator__step',
    props.className
  );
  return (
    <div className={ClassNames}>
      {props.children}
    </div>
  );
};

CalculatorStep.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

export default CalculatorStep;

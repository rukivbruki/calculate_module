import PropTypes from 'prop-types';
import React from 'react';
import cn from 'classnames';

const StepTitle = (props) => {
  const ClassNames = cn(
    'section-calculator__step-title',
    props.className
  );
  return (
    <h3 className={ClassNames} style={props.style}>
      {props.children}
    </h3>
  )
};

StepTitle.propTypes = {
  children: PropTypes.string.isRequired,
  className: PropTypes.string,
  style: PropTypes.object,
};

export default StepTitle;

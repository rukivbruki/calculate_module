export const HEIGHT_1 = '1.5 м';
export const HEIGHT_2 = '2 м';

export const WIDTH_1 = '3 м';
export const WIDTH_2 = '3.5 м';
export const WIDTH_3 = '4 м';
export const WIDTH_4 = '4.5 м';
export const WIDTH_5 = '5 м';

export const TYPE_1 = 'Из сетки рабицы';
export const TYPE_2 = 'Из сетки ГИТТЕР';
export const TYPE_3 = 'Из профнастила';
export const TYPE_4 = 'Из штакетника';
export const TYPE_5 = 'Из поликарбоната';
export const TYPE_6 = 'Из горизонтального штакетника';
export const TYPE_7 = 'Из деревянного штакетника';
export const TYPE_8 = 'Из дерева(плетенка)';
export const TYPE_FIXINGS = 'Крепеж';
export const TYPE_STONE = 'Щебень фр.5-20, 50кг';
export const TYPE_SAND = 'Песок строительный, 50кг';
export const TYPE_CEMENT = 'Цемент м-400 , 50кг';
export const TYPE_LAGA = 'Лага 40х20х3000мм, ППК';

export const VARIANT_INSTALLATION_1 = 'Штакетник в один ряд';
export const VARIANT_INSTALLATION_2 = 'Штакетник в шахматном порядке';
export const VARIANT_INSTALLATION_3 = 'Гор. штакетник в один ряд';
export const VARIANT_INSTALLATION_4 = 'Гор. штакетник в шахматном порядке';

export const CATEGORY_POST = 'столб';

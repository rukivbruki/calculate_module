import {
  HEIGHT_1,
  HEIGHT_2,
} from './constants';


const type_7_8_materials = [
  {
    name: 'Штакетник деревянный 1500х125х20мм, покрытие пинотекс',
    prevImg: require('../images/preview/Полотно/Дер. штакет/topv2t1s2coreh.jpg'),
    img: require('../images/popup/Полотно/Дер. штакет/topv2t1s2coreh.jpg'),
    height: HEIGHT_1,
    unit: 'куб',
    price: 30000,
  },
  {
    name: 'Штакетник деревянный 2000х125х20мм, покрытие пинотекс',
    prevImg: require('../images/preview/Полотно/Дер. штакет/topv2t1s2coreh.jpg'),
    img: require('../images/popup/Полотно/Дер. штакет/topv2t1s2coreh.jpg'),
    height: HEIGHT_2,
    unit: 'куб',
    price: 40000,
  },
  {
    name: 'Штакетник деревянный 1500х125х20мм, без покрытия',
    prevImg: require('../images/preview/Полотно/Дер. штакет/topv2t1s2colha.jpg'),
    img: require('../images/popup/Полотно/Дер. штакет/topv2t1s2colha.jpg'),
    height: HEIGHT_1,
    unit: 'куб',
    price: 15000,
  },
  {
    name: 'Штакетник деревянный 2000х125х20мм, без покрытия',
    prevImg: require('../images/preview/Полотно/Дер. штакет/topv2t1s2colha.jpg'),
    img: require('../images/popup/Полотно/Дер. штакет/topv2t1s2colha.jpg'),
    height: HEIGHT_2,
    unit: 'куб',
    price: 20000,
  },
  {
    name: 'Штакетник деревянный (плетенка) 1500х125х20мм, покрытие пинотекс',
    prevImg: require('../images/preview/Полотно/Дер. штакет/topv2t1s2coreh.jpg'),
    img: require('../images/popup/Полотно/Дер. штакет/topv2t1s2coreh.jpg'),
    height: HEIGHT_1,
    unit: 'куб',
    price: 30000,
  },
  {
    name: 'Штакетник деревянный (плетенка) 2000х125х20мм, покрытие пинотекс',
    prevImg: require('../images/preview/Полотно/Дер. штакет/topv2t1s2coreh.jpg'),
    img: require('../images/popup/Полотно/Дер. штакет/topv2t1s2coreh.jpg'),
    height: HEIGHT_2,
    unit: 'куб',
    price: 40000,
  },
  {
    name: 'Штакетник деревянный (плетенка) 1500х125х20мм, без покрытия',
    prevImg: require('../images/preview/Полотно/Дер. штакет/topv2t1s2colha.jpg'),
    img: require('../images/popup/Полотно/Дер. штакет/topv2t1s2colha.jpg'),
    height: HEIGHT_1,
    unit: 'куб',
    price: 15000,
  },
  {
    name: 'Штакетник деревянный (плетенка) 2000х125х20мм, без покрытия',
    prevImg: require('../images/preview/Полотно/Дер. штакет/topv2t1s2colha.jpg'),
    img: require('../images/popup/Полотно/Дер. штакет/topv2t1s2colha.jpg'),
    height: HEIGHT_2,
    unit: 'куб',
    price: 20000,
  },
];

export default type_7_8_materials;

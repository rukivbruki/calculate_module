import {
  HEIGHT_1,
  HEIGHT_2,
} from './constants';


const type_4_6_7_8_materials = [
  {
    name: 'Штакетник металлический 1500х136х0,5мм, двухсторонний полиэстр',
    prevImg: require('../images/preview/Полотно/Металлоштакет/shtaket-2.jpg'),
    img: require('../images/popup/Полотно/Металлоштакет/shtaket-2.jpg'),
    height: HEIGHT_1,
    unit: 'шт',
    price: 143,
  },
  {
    name: 'Штакетник металлический 1500х136х0,5мм, 1-сторонний полиэстр',
    prevImg: require('../images/preview/Полотно/Металлоштакет/shtaket-1.jpg'),
    img: require('../images/popup/Полотно/Металлоштакет/shtaket-1.jpg'),
    height: HEIGHT_1,
    unit: 'шт',
    price: 135,
  },
  {
    name: 'Штакетник металлический 2000х136х0,5мм, двухсторонний полиэстр',
    prevImg: require('../images/preview/Полотно/Металлоштакет/shtaket-2.jpg'),
    img: require('../images/popup/Полотно/Металлоштакет/shtaket-2.jpg'),
    height: HEIGHT_2,
    unit: 'шт',
    price: 190,
  },
  {
    name: 'Штакетник металлический 2000х136х0,5мм, 1-сторонний полиэстр',
    prevImg: require('../images/preview/Полотно/Металлоштакет/shtaket-1.jpg'),
    img: require('../images/popup/Полотно/Металлоштакет/shtaket-1.jpg'),
    height: HEIGHT_2,
    unit: 'шт',
    price: 180,
  },
];

export default type_4_6_7_8_materials;

import type_1 from './type_1';
import type_2 from './type_2';
import type_3 from './type_3';
import type_4 from './type_4';
import type_5 from './type_5';
import type_6 from './type_6';
import type_7 from './type_7';
import type_8 from './type_8';

const entities = {
  ...type_1,
  ...type_2,
  ...type_3,
  ...type_4,
  ...type_5,
  ...type_6,
  ...type_7,
  ...type_8,
};

export default entities;
